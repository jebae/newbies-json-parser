//
// Created by 배정모 on 2017. 11. 30..
//

#include "parse.h"

PTOKEN init(char *buffer, int *pos){
    PTOKEN json = malloc(sizeof(TOKEN));
    memset(json, 0, sizeof(TOKEN));
    char opener = buffer[*pos];

    switch (opener) {
        case '{':
            json->type = TOKEN_OBJ;
            break;
        case '[':
            json->type = TOKEN_ARR;
            break;
    }
    json->isEnd = true;
    return json;
}

void finalize(PTOKEN token) {
    if (token != NULL){
        switch (token->type) {
            case TOKEN_STR:
                free(token->str);
                break;

            default:
                break;
        }

        if (token->type == TOKEN_ARR || token->type == TOKEN_OBJ){
            for (int i=0; i < TOKEN_COUNT; i++) {
                if (token->tokens[i]->isEnd == true){
                    finalize(token->tokens[i]);
                    break;
                }
                finalize(token->tokens[i]);
            }
        }
        free(token);
    }
}

int parse(PTOKEN parentTok, char *buffer, int size, int *pos) {
    int tIndex = 0;
    char closer;

    if (parentTok->type == TOKEN_ARR) {
        closer = ']';
    } else if (parentTok->type == TOKEN_OBJ) {
        closer = '}';
    }

    while (size > *pos) {
        PTOKEN tok;
        bool added = false;
        *pos = *pos + 1;
        if (buffer[*pos] == closer) {
            parentTok->tokens[tIndex-1]->isEnd = true;
            return 0;
        }


        switch (buffer[*pos]) {
            case '"':
                tok = parseStr(buffer, pos);
                if (tok == NULL)
                {
                    printf ("Type Error : string is incomplete on pos %d\n", *pos);
                    return -1;
                }
                added = true;
                break;

            case '0':case '1':case '2':case '3':case '4':case '5':
            case '6':case '7':case '8':case '9':case '-':
                tok = parseNum(buffer, pos);
                if (tok == NULL)
                {
                    printf ("Type Error : number is incomplete on pos %d\n", *pos);
                    return -1;
                }
                added = true;
                break;

            case '[':
                tok = handleRecursion(buffer, size, pos, TOKEN_ARR);
                if (tok == NULL)
                    return -1;
                added = true;
                break;

            case '{':
                tok = handleRecursion(buffer, size, pos, TOKEN_OBJ);
                if (tok == NULL)
                    return -1;
                added = true;
                break;
        }
        if (added){
            parentTok->tokens[tIndex] = tok;
            tIndex++;
        }
    }
    return 0;
}

PTOKEN parseStr(char *buffer, int *pos) {
    PTOKEN tok = malloc(sizeof(TOKEN));
    memset(tok, 0, sizeof(TOKEN));
    char *begin, *end;
    int strLength;

    begin = buffer + *pos + 1;
    end = strchr(begin, '"');
    if (end == NULL)
        return NULL;
    strLength = end - begin;
    tok->str = malloc(strLength + 1);
    memset(tok->str, 0, strLength + 1);
    memcpy(tok->str, begin, strLength);
    *pos = *pos + strLength + 1;
    tok->type = TOKEN_STR;
    return tok;
}

PTOKEN parseNum(char *buffer, int *pos) {
    PTOKEN tok = malloc(sizeof(TOKEN));
    memset(tok, 0, sizeof(TOKEN));
    char *begin, *end = NULL;
    char *numBuffer;
    char closer[] = { ',', '}', ']' };
    int strLength;

    begin = buffer + *pos;

    for (int i=0; i < sizeof(closer) / sizeof(char); i++)
    {
        char *closerEnd = strchr(begin, closer[i]);
        if (closerEnd && (closerEnd < end | end == NULL)){
            end = closerEnd;
        }
    }
    if (end == NULL)
        return NULL;
    strLength = end - begin;
    numBuffer = malloc(strLength + 1);
    memset(numBuffer, 0, strLength + 1);
    memcpy(numBuffer, begin, strLength);
    tok->num = atoi(numBuffer);
    free(numBuffer);
    *pos = *pos + strLength - 1;
    tok->type = TOKEN_NUM;
    return tok;
}

PTOKEN handleRecursion(char *buffer, int size, int *pos, int TYPE) {
    PTOKEN tok = malloc(sizeof(TOKEN));
    memset(tok, 0, sizeof(TOKEN));
    tok->type = TYPE;
    if (parse(tok, buffer, size, pos) == -1) {
        printf ("Type Error: Obj or List on pos %d\n", *pos);
        return NULL;
    }
    return tok;
}







