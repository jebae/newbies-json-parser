//
// Created by 배정모 on 2017. 11. 30..
//

#ifndef PJSON_FILE_H
#define PJSON_FILE_H
#include <stdio.h>

int getFileSize(FILE *fp);
char *getBuffer(FILE *fp, int *size);

#endif //PJSON_FILE_H
