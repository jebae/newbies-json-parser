//
// Created by 배정모 on 2017. 12. 1..
//
#include "search.h"
#include "writeJson.h"

void *search(PTOKEN token, char *key, int *type) {
    void *ptr;
    int i = 0;

    if (token->type != TOKEN_OBJ)
        return NULL;
    while (1) {
        if (i % 2 == 0 && token->tokens[i]->type == TOKEN_STR && strcmp(token->tokens[i]->str, key) == 0) {
            *type = token->tokens[i+1]->type;
            switch (token->tokens[i+1]->type) {
                case TOKEN_STR:
                    ptr = token->tokens[i+1]->str;
                    return ptr;

                case TOKEN_NUM:
                    ptr = &token->tokens[i+1]->num;
                    return ptr;

                case TOKEN_ARR:
                    ptr = token->tokens[i+1];
                    return ptr;

                case TOKEN_OBJ:
                    ptr = token->tokens[i+1];
                    return ptr;
            }
        }
        if (token->tokens[i]->isEnd == true)
            break;
        i++;
    }
    return NULL;
}

void print(void *ptr, int type) {
    switch (type) {
        case TOKEN_STR:
            printf ("%s\n", (char *)ptr);
            break;

        case TOKEN_NUM:
            printf ("%.0lf\n", *(double *)ptr);
            break;

        case TOKEN_ARR:
            write((PTOKEN)ptr, stdout, 0);
            break;

        case TOKEN_OBJ:
            write((PTOKEN)ptr, stdout, 0);
            break;
    }
}