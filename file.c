//
// Created by 배정모 on 2017. 11. 30..
//

#include "file.h"
#include <stdlib.h>
#include <memory.h>

int getFileSize(FILE *fp){
    fseek(fp, 0, SEEK_END);
    int size = ftell(fp);
    fseek(fp, 0, SEEK_SET);
    return size;
}

char *getBuffer(FILE *fp, int *size) {
    *size = getFileSize(fp);
    char *buffer = malloc(*size + 1);
    memset(buffer, 0, *size + 1);
    fread(buffer, *size, 1, fp);
    return buffer;
}