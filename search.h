//
// Created by 배정모 on 2017. 12. 1..
//

#ifndef PJSON_SEARCH_H
#define PJSON_SEARCH_H
#include "token.h"

void *search(PTOKEN token, char *key, int *type);
void print(void *ptr, int type);

#endif //PJSON_SEARCH_H
