//
// Created by 배정모 on 2017. 12. 1..
//

#ifndef PJSON_WRITE_H
#define PJSON_WRITE_H
#include "token.h"

void write(PTOKEN token, FILE *fp, int depth);
void writeStr(PTOKEN token, FILE *fp);
void writeNum(PTOKEN token, FILE *fp);
void tab(int count, FILE *fp);

#endif //PJSON_WRITE_H
