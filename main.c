#include <stdio.h>
#include "file.h"
#include "parse.h"
#include "writeJson.h"
#include "search.h"

int main(int argc, char *argv[]) {
    if (argc == 1){
        printf ("Command list : \n");
        printf ("\tparse <filename>\n");
        printf ("\tsearch <filename> <key>\n");
        printf ("\twrite <filename> <output filename>\n");
        return 0;
    }

    char command = argv[1][0];
    char *filename = argv[2];
    char *key = argv[3];
    char *output = argv[3];

    int ret;
    FILE *doc;
    FILE *fp;
    int size;
    int type;
    int pos = 0;
    char *buffer;
    void *ptr;

    doc = fopen(filename, "rb");
    buffer = getBuffer(doc, &size);

    PTOKEN json = init(buffer, &pos);
    ret = parse(json, buffer, size, &pos);

    if (ret == 0) {
        switch (command) {
            case 'p':
                write(json, stdout, 0);
                break;

            case 's':
                ptr = search(json, key, &type);
                print (ptr, type);
                break;

            case 'w':
                fp = fopen(output, "wb");
                write(json, fp, 0);
                fclose(fp);
                break;

            default:
                break;
        }
    }

    finalize(json);
    free(buffer);
    fclose(doc);
    return 0;
}