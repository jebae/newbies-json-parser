//
// Created by 배정모 on 2017. 12. 1..
//
#include "writeJson.h"

void write(PTOKEN token, FILE *fp, int depth) {
    int i;
    switch (token->type) {
        case TOKEN_STR:
            writeStr(token, fp);
            break;

        case TOKEN_NUM:
            writeNum(token, fp);
            break;

        case TOKEN_ARR:
            fwrite("[\n", 2, 1, fp);
            i = 0;
            while (1) {
                tab(depth + 1, fp);
                write(token->tokens[i], fp, depth + 1);
                if (token->tokens[i]->isEnd == true)
                    break;
                i++;
            }
            tab(depth, fp);
            fwrite("]", 1, 1, fp);
            break;

        case TOKEN_OBJ:
            fwrite("{\n", 2, 1, fp);
            i = 0;
            while (1) {
                if (i % 2) {
                    write(token->tokens[i], fp, depth + 1);
                } else {
                    tab(depth + 1, fp);
                    writeStr(token->tokens[i], fp);
                    fwrite(" : ", 3, 1, fp);
                }
                if (token->tokens[i]->isEnd == true)
                    break;
                i++;
            }
            tab(depth, fp);
            fwrite("}", 1, 1, fp);
            break;


    }
    if (token->isEnd == false)
        fwrite(",\n", 2, 1, fp);
    else
        fwrite("\n", 1, 1, fp);
}

void writeStr(PTOKEN token, FILE *fp) {
    fwrite("\"", 1, 1, fp);
    fwrite(token->str, strlen(token->str), 1, fp);
    fwrite("\"", 1, 1, fp);
}

void writeNum(PTOKEN token, FILE *fp) {
    char num[100];
    sprintf (num, "%.0lf", token->num);
    fwrite(num, strlen(num), 1, fp);
}

void tab(int count, FILE *fp) {
    for (int i=0; i < count; i++) {
        fwrite("\t", 1, 1, fp);
    }
}