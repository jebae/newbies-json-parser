//
// Created by 배정모 on 2017. 11. 30..
//

#ifndef PJSON_TOKEN_H_H
#define PJSON_TOKEN_H_H
#define TOKEN_COUNT 20

#include <stdbool.h>
#include <stdlib.h>
#include <memory.h>
#include <stdio.h>

typedef enum _TOKEN_TYPE{
    TOKEN_STR,
    TOKEN_NUM,
    TOKEN_ARR,
    TOKEN_OBJ,
} TOKEN_TYPE;

#pragma pack(push, 1)
typedef struct _TOKEN {
    TOKEN_TYPE type;
    bool isEnd;
    union {
        char *str;
        double num;
        struct _TOKEN *tokens[TOKEN_COUNT];
    };
} TOKEN, *PTOKEN;
#pragma pack(pop)

#endif //PJSON_TOKEN_H_H
