//
// Created by 배정모 on 2017. 11. 30..
//

#ifndef PJSON_PARSE_H
#define PJSON_PARSE_H
#include "token.h"

PTOKEN init(char *buffer, int *pos);
void finalize(PTOKEN json);
PTOKEN parseStr(char *buffer, int *pos);
PTOKEN parseNum(char *buffer, int *pos);
PTOKEN handleRecursion(char *buffer, int size, int *pos, int TYPE);
int parse(PTOKEN parentTok, char *buffer, int size, int *pos);

#endif //PJSON_PARSE_H
